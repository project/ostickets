<?php

namespace Drupal\ostickets\Plugin\WebformHandler;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\ostickets\OSTicketsClientInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission action handler.
 *
 * @WebformHandler(
 *   id = "ostickets",
 *   label = @Translation("OS Tickets: Create Ticket"),
 *   category = @Translation("Action"),
 *   description = @Translation("POST a ticket upon submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class OSTicketsWebformHandler extends WebformHandlerBase {

  const BASE_FIELDS = [
    'email' => ['label' => 'Email', 'allowed_types' => NULL, 'allow_custom' => TRUE],
    'name' => ['label' => 'Name', 'allowed_types' => NULL, 'allow_custom' => TRUE],
    'subject' => ['label' => 'Subject', 'allowed_types' => NULL, 'allow_custom' => TRUE],
    'message' => ['label' => 'Message', 'allowed_types' => NULL, 'allow_custom' => TRUE],
  ];
  const ADDITONAL_FIELDS = [
    'priority' => ['label' => 'Priority', 'allowed_types' => ['select'], 'allow_custom' => TRUE],
    'attachments' => ['label' => 'Attachments', 'allowed_types' => ['managed_file', 'webform_image_file', 'webform_document_file']],
    'topicId' => ['label' => 'Topic ID', 'allowed_types' => ['select'], 'allow_custom' => TRUE],
  ];

  /**
   * The webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The OS Tickets client
   *
   * @var \Drupal\ostickets\OSTicketsClientInterface
   */
  protected $osTicketsClient;

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module handler
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The uuid generator
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The current weight
   *
   * @var int
   */
  protected $currentWeight = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, WebformTokenManagerInterface $token_manager, OSTicketsClientInterface $ostickets_client, FileSystemInterface $file_system, ModuleHandlerInterface $module_handler, UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->tokenManager = $token_manager;
    $this->osTicketsClient = $ostickets_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('webform.token_manager'),
      $container->get('ostickets.client'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $settings = [
      'hostname' => '',
      'api_key' => '',
      'secure' => TRUE,
      'custom_fields' => [],
    ];

    $all_fields = self::BASE_FIELDS + self::ADDITONAL_FIELDS;

    foreach ($all_fields as $key => $field) {
      $settings["{$key}_field"] = '';
      if ($field['allow_custom']) {
        $settings["{$key}_custom"] = '';
      }
    }

    return $settings;
  }

  /**
   * Gets a configuration by key
   *
   * @param string $key
   *
   * @return mixed
   */
  private function getConfigurationKey($key) {
    $configuration = $this->getConfiguration()['settings'];
    return isset($configuration[$key]) ? $configuration[$key] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->elementTokenValidate($form);
    $custom_fields = $form_state->get('custom_fields');
    if (!is_array($custom_fields)) {
      $custom_fields = $this->getConfigurationKey('custom_fields');
    }

    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#default_value' => $this->getConfigurationKey('hostname'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->getConfigurationKey('api_key'),
    ];

    $form['secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Secure (Use HTTPS)'),
      '#default_value' => $this->getConfigurationKey('secure'),
    ];

    $all_fields = self::BASE_FIELDS + self::ADDITONAL_FIELDS;
    foreach ($all_fields as $key => $field) {
      $this->buildElement($key, $field, $form, in_array($key, array_keys(self::BASE_FIELDS)));
    }

    $form['custom_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Fields'),
      '#tree' => TRUE,
    ];

    $custom_fields_container = &$form['custom_fields'];

    foreach ($custom_fields as $custom_field) {
      $this->buildCustomElement($custom_fields_container, $custom_field['uuid'], $custom_field);
    }

    $form['add_custom_field'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Custom Field'),
      '#submit' => [[$this, 'addCustomField']],
      '#limit_validation_errors' => [],
      '#uuid' => $custom_field['uuid'],
      '#ajax' => [
        'wrapper' => "webform-tab--general",
        'callback' => [$this, 'renderGeneralTab'],
      ],
    ];

    return $this->setSettingsParents($form);
  }

  public function removeCustomField($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $uuid = $trigger["#parents"][2];
    $user_inputs = $form_state->getUserInput();
    $custom_fields = isset($user_inputs["settings"]["custom_fields"]) ? $user_inputs["settings"]["custom_fields"] : [];
    unset($custom_fields[$uuid]);
    $form_state->set('custom_fields', $custom_fields);
    $form_state->setRebuild();
  }

  /**
   * Adds a custom field
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return void
   */
  public function addCustomField($form, FormStateInterface $form_state) {
    $user_inputs = $form_state->getUserInput();
    $custom_fields = isset($user_inputs["settings"]["custom_fields"]) ? $user_inputs["settings"]["custom_fields"] : [];
    $uuid = $this->uuid->generate();
    $custom_fields[$uuid] = [
      'uuid' => $uuid,
      'osticket_id' => '',
      'webform_field' => '',
      'custom' => '',
    ];
    $form_state->set('custom_fields', $custom_fields);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback to render the general tab
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function renderGeneralTab($form, FormStateInterface $form_state) {
    return $form['tab_general'];
  }

  protected function buildCustomElement(&$container, $uuid, $values) {
    $options = $this->getWebformElementsAsOptions();
    $container[$uuid] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Field'),
      '#tree' => TRUE,
      'osticket_id' => [
        '#type' => 'textfield',
        '#title' => $this->t('OS Ticket Variable ID'),
        '#default_value' => isset($values['osticket_id']) ? $values['osticket_id'] : NULL,
        '#required' => TRUE,
      ],
      'webform_field' => [
        '#type' => 'select',
        '#title' => $this->t('Field'),
        '#options' => $options,
        '#default_value' => isset($values['webform_field']) ? $values['webform_field'] : NULL,
        '#required' => TRUE,
      ],
      'custom' => [
        '#type' => 'textfield',
        '#title' => $this->t('Custom Value'),
        '#default_value' => isset($values['custom']) ? $values['custom'] : NULL,
        '#states' => [
          'visible' => [
            "select[name='settings[custom_fields][$uuid][webform_field]'" => [
              ['value' => '_token'],
              'or',
              ['value' => '_custom'],
            ],
          ],
        ],
      ],
      'uuid' => [
        '#type' => 'hidden',
        '#default_value' => $uuid,
      ],
      'remove' => [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => [[$this, 'removeCustomField']],
        '#limit_validation_errors' => [],
        '#name' => 'submit' . $uuid,
        '#ajax' => [
          'wrapper' => "webform-tab--general",
          'callback' => [$this, 'renderGeneralTab'],
        ],
      ],
    ];
  }

  /**
   * Gets the allowed elements inside the webform
   *
   * @param NULL|array $allowed_types
   *   The allowed types.
   *
   * @return array
   *   The options.
   */
  protected function getWebformElementsAsOptions($allowed_types = NULL, $allow_custom = TRUE) {
    $elements = $this->webform->getElementsDecodedAndFlattened();

    $options = array_filter($elements, function ($element) use ($allowed_types) {
      if (empty($allowed_types)) {
        return $element['#type'] != 'captcha';
      }

      if (is_array($allowed_types)) {
        return in_array($element['#type'], $allowed_types);
      }
    });

    $options = array_map(function ($element) {return $element['#title'];}, $options);

    if ($allow_custom) {
      $options['_token'] = $this->t('- Use Token -');
      $options['_custom'] = $this->t('- Custom -');
    }

    return $options;
  }

  /**
   * Builds an element with options.
   *
   * @param string $key
   * @param string $label
   * @param array $form
   * @param boolean $required
   *
   * @return array
   */
  protected function buildElement($key, $field, &$form, $required = TRUE) {

    $options = $this->getWebformElementsAsOptions($field['allowed_types'], $field['allow_custom']);

    $container = [
      '#type' => 'container',
      '#tree' => FALSE,
    ];
    $container["{$key}_field"] = [
      '#type' => 'select',
      '#title' => $field['label'],
      '#options' => $options,
      '#empty_option' => $required ? NULL : $this->t('- None -'),
      '#required' => $required,
      '#default_value' => $this->getConfigurationKey("{$key}_field"),
    ];

    if ($field['allow_custom']) {
      $container["{$key}_custom"] = [
        '#type' => 'textfield',
        '#title' => $this->t("@label (Custom)", ['@label' => $field['label']]),
        '#states' => [
          'visible' => [
            "select[name='settings[{$key}_field]'" => [
              ['value' => '_token'],
              'or',
              ['value' => '_custom'],
            ],
          ],
        ],
        '#default_value' => $this->getConfigurationKey("{$key}_custom"),
      ];
    }

    $form["{$key}_container"] = $container;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

    // Validate data element keys.
    $elements = $this->getWebform()->getElementsInitializedFlattenedAndHasValue();
    $data = Yaml::decode($form_state->getValue('data')) ?: [];
    foreach ($data as $key => $value) {
      if (!isset($elements[$key])) {
        $form_state->setErrorByName('data', $this->t('%key is not valid element key.', ['%key' => $key]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);

    // Cleanup states.
    $this->configuration['states'] = array_values(array_filter($this->configuration['states']));

    // Cleanup sticky.
    if ($form_state->getValue('sticky') === '') {
      $this->configuration['sticky'] = NULL;
    }

    // Cleanup locked.
    if ($form_state->getValue('locked') === '') {
      $this->configuration['locked'] = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $webform_data = $webform_submission->getData();
    $data_to_send = [];
    $settings = $this->getConfiguration()['settings'];
    $custom_field_keys = [];

    // Build custom fields to be like normal fields.
    if (is_array($settings['custom_fields'])) {
      foreach ($settings['custom_fields'] as $uuid => $field) {
        $osticket_id = $field['osticket_id'];
        $webform_field = $field['webform_field'];
        $custom = $field['custom'];

        $custom_field_keys[] = $osticket_id;
        $settings["{$osticket_id}_field"] = $webform_field;
        $settings["{$osticket_id}_custom"] = $custom;
      }
    }

    foreach (array_merge(array_keys(self::BASE_FIELDS + self::ADDITONAL_FIELDS), $custom_field_keys) as $key) {
      // Ignore the attachments field for now.
      if ($key == 'attachments') {
        continue;
      }

      // Get the field name.
      $field_name = $settings["{$key}_field"];
      if ($field_name == '_token') {
        $value = $this->tokenManager->replace($this->getConfigurationKey("{$key}_custom"), $webform_submission);
        if (!empty($value)) {
          $data_to_send[$key] = $value;
        }
      } else if ($field_name == '_custom') {
        $value = $settings["{$key}_custom"];
        if (!empty($value)) {
          $data_to_send[$key] = $value;
        }
      } else {
        if (!empty($webform_data[$field_name])) {
          $data_to_send[$key] = $webform_data[$field_name];
        }
      }
    }
    $attachments_key = $settings['attachments_field'];
    if ($attachments_key && isset($webform_data[$attachments_key])) {
      if (is_int($webform_data[$attachments_key])) {
        $webform_data[$attachments_key] = [$webform_data[$attachments_key]];
      }

      foreach ($webform_data[$attachments_key] as $file_id) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $this->entityTypeManager->getStorage('file')->load($file_id);
        $path = $this->fileSystem->realpath($file->getFileUri());
        $data = file_get_contents($path);
        $encoding = 'base64';
        $data = base64_encode($data);
        $data_to_send['attachments'][] = [$file->getFilename() => 'data:' . $file->getMimeType() . ';' . $encoding . ',' . mb_convert_encoding($data, 'UTF-8', 'UTF-8')];

      }
    }

    $this->osTicketsClient->setAPIKey($this->getConfigurationKey('api_key'));
    $this->osTicketsClient->setHostName($this->getConfigurationKey('hostname'));
    $this->osTicketsClient->setSecure($this->getConfigurationKey('secure'));
    try {
      // Alter the data before sending.
      $this->moduleHandler->alter('osticket_data', $data_to_send, $webform_submission, $this->osTicketsClient);
      $this->osTicketsClient->sendTicket($data_to_send);
    } catch (GuzzleException $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

}
