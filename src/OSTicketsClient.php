<?php

namespace Drupal\ostickets;

use Drupal\ostickets\Exception\OSTicketsConfigurationException;
use GuzzleHttp\Client;

/**
 * Class OSTicketsClient.
 */
class OSTicketsClient implements OSTicketsClientInterface {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The API key
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The hostname
   *
   * @var string
   */
  protected $hostname;

  /**
   * Use https
   *
   * @var bool
   */
  protected $secure = TRUE;

  /**
   * Constructs a new OSTicketsClient object.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritDoc}
   */
  public function setAPIKey($api_key) {
    $this->apiKey = $api_key;
  }

  /**
   * {@inheritDoc}
   */
  public function setSecure($secure = TRUE) {
    $this->secure = $secure;
  }

  /**
   * {@inheritDoc}
   */
  public function setHostName($hostname) {
    $this->hostname = $hostname;
  }

  /**
   * Gets the hostname
   *
   * @return string
   */
  protected function getHostName() {
    return trim(trim($this->hostname), '/');
  }

  /**
   * {@inheritDoc}
   */
  public function sendTicket(array $data) {
    if (!isset($this->apiKey)) {
      throw new OSTicketsConfigurationException('The API key is not properly configured.');
    }

    if (!isset($this->hostname)) {
      throw new OSTicketsConfigurationException('The hostname is not properly configured.');
    }

    $uri = ($this->secure ? 'https' : 'http') . '://' . $this->getHostName() . '/api/tickets.json';

    return $this->httpClient->post($uri, [
      'json' => $data,
      'verify' => FALSE,
      'headers' => [
        'X-API-Key' => $this->apiKey,
      ],
    ]);
  }

}
