<?php

namespace Drupal\ostickets;

/**
 * Interface OSTicketsClientInterface.
 */
interface OSTicketsClientInterface {
  /**
   * Sets the api key.
   *
   * @param string $api_key
   *    The API key.
   * @return OSTicketsClientInterface
   */
  public function setAPIKey($api_key);

  /**
   * Sets the hostname
   *
   * @param string $hostname
   *    The hostname.
   *
   * @return OSTicketsClientInterface
   */
  public function setHostName($hostname);

  /**
   * {@inheritDoc}
   */
  public function setSecure($secure = TRUE);

  /**
   * Sends a ticket
   *
   * @param array $data
   *    The data.
   *
   * @throws \Drupal\ostickets\Exception\OSTicketsConfigurationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function sendTicket(array $data);

}
