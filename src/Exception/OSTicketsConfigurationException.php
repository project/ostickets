<?php

namespace Drupal\ostickets\Exception;

/**
 * Exception for invalid OS Tickets configuration.
 * 
 */
class OSTicketsConfigurationException extends \Exception {}